class Identifier:
    __login: str | None

    def __init__(self, *, login: str | None = None) -> None:
        self.__login = login

    def is_empty(self) -> bool:
        return self.__login is None

    def __eq__(self, other: 'Identifier') -> bool:
        if not isinstance(other, Identifier):
            raise TypeError
        return hash(self) == hash(other)

    def __hash__(self) -> int:
        return hash((self.__login))
