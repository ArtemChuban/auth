from . import (
    CantGetIdentifier,
    WrongSecretException,
    CantAuthentificateException,
    UserDoesntExistException,
    UserExistException,
    CantRegisterException,
    EmptyIdentifierException,
    Identifier,
    Token,
    Secret,
    User,
    UsersCollection
)


class Auth:
    __usersCollection: UsersCollection

    def __init__(self) -> None:
        self.__usersCollection = UsersCollection()

    def try_register(self, identifier: Identifier, secret: Secret) -> None:
        try:
            self._register(identifier, secret)
        except EmptyIdentifierException as exc:
            raise CantRegisterException from exc
        except UserExistException as exc:
            raise CantRegisterException from exc

    def try_authentificate(self,
                           identifier: Identifier,
                           secret: Secret) -> Token:
        try:
            return self._authentificate(identifier, secret)
        except UserDoesntExistException as exc:
            raise CantAuthentificateException from exc
        except WrongSecretException as exc:
            raise CantAuthentificateException from exc

    def try_get_identifier_by_token(self, token: Token) -> Identifier:
        try:
            return self._get_identifier_by_token(token)
        except UserDoesntExistException as exc:
            raise CantGetIdentifier from exc

    def _get_identifier_by_token(self, token: Token) -> Identifier:
        return self.__usersCollection.get_identifier_by_token(token)

    def _register(self, identifier: Identifier, secret: Secret) -> None:
        new_user = User(identifier, secret)
        self.__usersCollection.append(new_user)

    def _authentificate(self, identifier: Identifier, secret: Secret) -> Token:
        user = self.__usersCollection.get(identifier)
        if user.secret != secret:
            raise WrongSecretException
        return self.__usersCollection.generate_and_return_token(identifier)
