from uuid import uuid4


class Token:
    __value: str

    def __init__(self, value: str | None = None) -> None:
        if value:
            self.__value = value
        else:
            self.__value = self._random_value()

    def __eq__(self, other: 'Token') -> bool:
        if not isinstance(other, Token):
            raise TypeError
        return hash(self) == hash(other)

    def __hash__(self) -> int:
        return hash((self.__value))

    @staticmethod
    def _random_value() -> str:
        return str(uuid4())
