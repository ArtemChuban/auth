class UserExistException(Exception):
    pass


class CantRegisterException(Exception):
    pass


class EmptyIdentifierException(Exception):
    pass


class CantAuthentificateException(Exception):
    pass


class UserDoesntExistException(Exception):
    pass


class WrongSecretException(Exception):
    pass


class CantGetIdentifier(Exception):
    pass
