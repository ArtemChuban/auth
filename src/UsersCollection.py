from . import (
    UserDoesntExistException,
    UserExistException,
    Token,
    Identifier,
    User
)


class UsersCollection:
    __users: list[User]

    def __init__(self) -> None:
        self.__users = list()

    def exist(self, identifier: Identifier) -> bool:
        for user in self.__users:
            if user.identifier == identifier:
                return True
        return False

    def append(self, user: User) -> None:
        if self.exist(user.identifier):
            raise UserExistException
        self.__users.append(user)

    def get_identifier_by_token(self, token: Token) -> Identifier:
        for user in self.__users:
            if user.have_token(token):
                return user.identifier
        raise UserDoesntExistException

    def get(self, identifier: Identifier) -> User:
        for user in self.__users:
            if user.identifier == identifier:
                return user
        raise UserDoesntExistException

    def generate_and_return_token(self, identifier: Identifier) -> Token:
        for i, user in enumerate(self.__users):
            if user.identifier == identifier:
                token = Token()
                self.__users[i].append_token(token)
                return token
        raise UserDoesntExistException
