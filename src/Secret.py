class Secret:
    __password: str | None

    def __init__(self, *, password: str | None = None) -> None:
        self.__password = password

    def is_empty(self) -> bool:
        return self.__password is None

    def __eq__(self, other: 'Secret') -> bool:
        if not isinstance(other, Secret):
            raise TypeError
        return hash(self) == hash(other)

    def __hash__(self) -> int:
        return hash((self.__password))
