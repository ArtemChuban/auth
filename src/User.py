from . import EmptyIdentifierException, Token, Identifier, Secret


class User:
    __identifier: Identifier
    __secret: Secret
    __tokens: list[Token]

    def __init__(self,
                 identifier: Identifier,
                 secret: Secret,
                 tokens: list[Token] = list()) -> None:
        if identifier.is_empty():
            raise EmptyIdentifierException
        self.__identifier = identifier
        self.__secret = secret
        self.__tokens = tokens

    @property
    def identifier(self) -> Identifier:
        return self.__identifier

    @property
    def secret(self) -> Secret:
        return self.__secret

    def append_token(self, token: Token) -> None:
        self.__tokens.append(token)

    def have_token(self, token: Token) -> bool:
        return token in self.__tokens
