import unittest
from src import UserDoesntExistException, UserExistException, Identifier, Secret, User, UsersCollection


class TestUsersCollection(unittest.TestCase):
    logins: list[str] = ["login", "name"]
    passwords: list[str] = ["password", "qwerty"]

    def test_append_success(self):
        usersCollection = UsersCollection()
        identifier = Identifier(login=self.logins[0])
        secret = Secret(password=self.passwords[0])
        user = User(identifier, secret)
        usersCollection.append(user)

    def test_append_twice(self):
        usersCollection = UsersCollection()
        identifier = Identifier(login=self.logins[0])
        secret = Secret(password=self.passwords[0])
        user = User(identifier, secret)
        usersCollection.append(user)
        self.assertRaises(UserExistException,
                          lambda: usersCollection.append(user))

    def test_get_success(self):
        usersCollection = UsersCollection()
        identifier = Identifier(login=self.logins[0])
        secret = Secret(password=self.passwords[0])
        user = User(identifier, secret)
        usersCollection.append(user)
        same_user = usersCollection.get(identifier)
        self.assertEqual(user.identifier, same_user.identifier)
        self.assertEqual(user.secret, same_user.secret)

    def test_get_not_existed(self):
        usersCollection = UsersCollection()
        identifier = Identifier(login=self.logins[0])
        self.assertRaises(UserDoesntExistException,
                          lambda: usersCollection.get(identifier))

    def test_exits_success(self):
        usersCollection = UsersCollection()
        identifier = Identifier(login=self.logins[0])
        secret = Secret(password=self.passwords[0])
        user = User(identifier, secret)
        usersCollection.append(user)
        self.assertTrue(usersCollection.exist(identifier))

    def test_not_exist(self):
        usersCollection = UsersCollection()
        identifier = Identifier(login=self.logins[0])
        self.assertFalse(usersCollection.exist(identifier))

    def test_generate_and_return_token_success(self):
        usersCollection = UsersCollection()
        identifier = Identifier(login=self.logins[0])
        secret = Secret(password=self.passwords[0])
        user = User(identifier, secret)
        usersCollection.append(user)
        usersCollection.generate_and_return_token(identifier)

    def test_generate_and_return_token_not_exist(self):
        usersCollection = UsersCollection()
        identifier = Identifier(login=self.logins[0])
        self.assertRaises(UserDoesntExistException,
                          lambda: usersCollection.generate_and_return_token(identifier))
