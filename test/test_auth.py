import unittest
from src import CantGetIdentifier, CantAuthentificateException, CantRegisterException, Token, Identifier, Secret, Auth


class TestAuth(unittest.TestCase):
    logins: list[str] = ["login", "name"]
    passwords: list[str] = ["password", "qwerty"]

    def test_get_identifier_by_token(self):
        auth = Auth()
        identifier = Identifier(login=self.logins[0])
        secret = Secret(password=self.passwords[0])
        auth.try_register(identifier, secret)
        token = auth.try_authentificate(identifier, secret)
        same_identifier = auth.try_get_identifier_by_token(token)
        self.assertEqual(identifier, same_identifier)

    def test_get_identifier_by_not_existed_token(self):
        auth = Auth()
        token = Token()
        self.assertRaises(CantGetIdentifier,
                          lambda: auth.try_get_identifier_by_token(token))

    def test_equal_identifiers_from_different_tokens(self):
        auth = Auth()
        identifier = Identifier(login=self.logins[0])
        secret = Secret(password=self.passwords[0])
        auth.try_register(identifier, secret)
        token1 = auth.try_authentificate(identifier, secret)
        token2 = auth.try_authentificate(identifier, secret)
        identifier1 = auth.try_get_identifier_by_token(token1)
        identifier2 = auth.try_get_identifier_by_token(token2)
        self.assertEqual(identifier1, identifier2)

    def test_empty_identifier_register(self):
        auth = Auth()
        identifier = Identifier()
        secret = Secret(password=self.passwords[0])
        self.assertRaises(CantRegisterException,
                          lambda: auth.try_register(identifier, secret))

    def test_register_twice(self):
        auth = Auth()
        identifier = Identifier(login=self.logins[0])
        secret1 = Secret(password=self.passwords[0])
        secret2 = Secret(password=self.passwords[1])
        auth.try_register(identifier, secret1)
        self.assertRaises(CantRegisterException,
                          lambda: auth.try_register(identifier, secret2))

    def test_register_success(self):
        auth = Auth()
        identifier = Identifier(login=self.logins[0])
        secret = Secret(password=self.passwords[0])
        auth.try_register(identifier, secret)

    def test_authentificate_success(self):
        auth = Auth()
        identifier = Identifier(login=self.logins[0])
        secret = Secret(password=self.passwords[0])
        auth.try_register(identifier, secret)
        auth.try_authentificate(identifier, secret)

    def test_authentificate_not_registered_user(self):
        auth = Auth()
        identifier = Identifier(login=self.logins[0])
        secret = Secret(password=self.passwords[0])
        self.assertRaises(CantAuthentificateException,
                          lambda: auth.try_authentificate(identifier, secret))

    def test_authentificate_with_wrong_secret(self):
        auth = Auth()
        identifier = Identifier(login=self.logins[0])
        correct_secret = Secret(password=self.passwords[0])
        wrong_secret = Secret(password=self.passwords[1])
        auth.try_register(identifier, correct_secret)
        self.assertRaises(CantAuthentificateException,
                          lambda: auth.try_authentificate(identifier,
                                                          wrong_secret))
