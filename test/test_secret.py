import unittest
from src import Secret


class TestSecret(unittest.TestCase):
    passwords: list[str] = ["password", "qwerty"]

    def test_wrong_type(self):
        secret = Secret(password=self.passwords[0])
        self.assertRaises(TypeError, lambda: secret == self.passwords[0])

    def test_different_secrets(self):
        secret_left = Secret(password=self.passwords[0])
        secret_right = Secret(password=self.passwords[1])
        self.assertNotEqual(secret_left, secret_right)

    def test_equals_secrets(self):
        secret_left = Secret(password=self.passwords[0])
        secret_right = Secret(password=self.passwords[0])
        self.assertEqual(secret_left, secret_right)

    def test_empty_secret(self):
        secret = Secret()
        self.assertTrue(secret.is_empty())

    def test_not_empty_secret(self):
        secret = Secret(password=self.passwords[0])
        self.assertFalse(secret.is_empty())
