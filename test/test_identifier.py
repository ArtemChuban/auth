import unittest
from src import Identifier


class TestIdentifier(unittest.TestCase):
    logins: list[str] = ["login", "name"]

    def test_wrong_type(self):
        identifier = Identifier(login=self.logins[0])
        self.assertRaises(TypeError, lambda: identifier == self.logins[0])

    def test_different_identifiers(self):
        identifier_left = Identifier(login=self.logins[0])
        identifier_right = Identifier(login=self.logins[1])
        self.assertNotEqual(identifier_left, identifier_right)

    def test_equals_identifiers(self):
        identifier_left = Identifier(login=self.logins[0])
        identifier_right = Identifier(login=self.logins[0])
        self.assertEqual(identifier_left, identifier_right)

    def test_empty_identifier(self):
        identifier = Identifier()
        self.assertTrue(identifier.is_empty())

    def test_not_empty_identifier(self):
        identifier = Identifier(login=self.logins[0])
        self.assertFalse(identifier.is_empty())
