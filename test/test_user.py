import unittest
from src import EmptyIdentifierException, Identifier, Secret, User


class TestUser(unittest.TestCase):
    logins: list[str] = ["login", "name"]
    passwords: list[str] = ["password", "qwerty"]

    def test_empty_identifier(self):
        empty_identifier = Identifier()
        secret = Secret(password=self.passwords[0])
        self.assertRaises(EmptyIdentifierException,
                          lambda: User(empty_identifier, secret))

    def test_check_identifier(self):
        identifier = Identifier(login=self.logins[0])
        secret = Secret(password=self.passwords[0])
        user = User(identifier, secret)
        self.assertEqual(user.identifier, identifier)
